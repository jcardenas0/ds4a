
Deploying flask con un dominio

https://www.digitalocean.com/community/questions/how-do-i-point-my-custom-domain-to-my-ip-port-41-111-20-36-8080


Hay un problema con digital ocean para el deploy de flask
El servicio se cae despues de un tiempo.

1. Hubo que montar un servicio  (deamon) para mantener el servicio activo.

Tiene los comando bash para iniciar el servicio
sudo nano start_wsgi.sh

permisos sobre el archivo
chmod +x

2. Se crea un archivo que contiene el servicio que activará el run del archivo wsgi.py

EL SERVICIO start_wsgi.service
ubicado en  /etc/systemd/system/

[Unit]
Description=Backend service
After=network.target

[Service]
Type=simple
Restart=always
RestartSec=1
User=root
ExecStart=/root/ds4a/ds4a/start_wsgi.sh

[Install]
WantedBy=multi-user.target

## reiniciar el servicio
 sudo systemctl restart start_wsgi : iniciar
 journalctl -x : errores